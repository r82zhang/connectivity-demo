Used sqlite3 as the database as it is a simple choice without me installing anything
and it is easy for you guys to run and verify the result

I created a table called hotel

example Excuation:
  python3 data_service.py -c abc -ci checkin -co checkout

using command line shell to verify:
  sqlite3 new_hotel_database.db
  select * from hotel;


dependency is just requests. See requirements.txt for details

Let me know if you guys having trouble running the code: r82zhang@uwaterloo.ca      

