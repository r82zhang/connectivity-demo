# -*- coding: utf-8 -*-
"""Used sqlite3 as it is easy
"""
import sqlite3
import os
from argparse import ArgumentParser
from multiprocessing import Process, Manager
import xml.etree.ElementTree as ET
import sys
import requests

JSON_ENDPOINT = 'https://experimentation.getsnaptravel.com/interview/hotels'
XML_ENDPOINT = 'https://experimentation.getsnaptravel.com/interview/legacy_hotels'
DB_NAME = "new_hotel_database.db"

# Check if table exists, if not, create one
SQL_CREATE_TABLE = """CREATE TABLE IF NOT EXISTS hotel (
                                    id integer PRIMARY KEY,
                                    hotel_name text NOT NULL,
                                    num_reviews integer,
                                    address text NOT NULL,
                                    num_stars integer NOT NULL,
                                    amenities text,
                                    image_url text NOT NULL,
                                    price float NOT NULL
                    );"""

SQL_INSERT_DATA = ''' INSERT INTO hotel(id, hotel_name, num_reviews, address,
                                        num_stars, amenities, image_url, price)
                      VALUES(?,?,?,?,?,?,?,?) '''

def create_json_data(city_string_input, checkin_string_input, checkout_string_input):
    """Constructs json post data"""
    return {
        'city': city_string_input,
        'checkin': checkin_string_input,
        'checkout': checkout_string_input,
        'provider': 'snaptravel'
    }

def create_xml_data(city_string_input, checkin_string_input, checkout_string_input):
    """Constructs xml post data"""
    return """
        <?xml version="1.0" encoding="UTF-8"?>
        <root>
          <checkin>{}</checkin>
          <checkout>{}</checkout>
          <city>{}</city>
          <provider>snaptravel</provider>
        </root>
    """.format(city_string_input, checkout_string_input, checkout_string_input)


def make_json_request(city_string_input, checkin_string_input, checkout_string_input, return_dict):
    """ making the request to json endpoint and save the response to return_dict"""
    j_data = create_json_data(city_string_input, checkin_string_input, checkout_string_input)
    headers = {'Content-Type': 'application/json'}
    r = requests.post(JSON_ENDPOINT, json=j_data, headers=headers)
    if r.status_code >= 400:
        print("error in making request to Json end point")
        print("status_code: {}".format(r.status_code))
        print("response body: {}".format(r.text))
        sys.exit()
    else:
        return_dict['json_response'] = r


def make_xml_request(city_string_input, checkin_string_input, checkout_string_input, return_dict):
    """ making the request to xml endpoint and save the response to return_dict"""
    x_data = create_xml_data(city_string_input, checkin_string_input, checkout_string_input)
    headers = {'Content-Type': 'application/xml'}
    r = requests.post(XML_ENDPOINT, data=x_data, headers=headers)
    if r.status_code >= 400:
        print("error in making request to XML end point")
        print("status_code: {}".format(r.status_code))
        print("response body: {}".format(r.text))
        sys.exit()
    else:
        return_dict["xml_response"] = r


def parse_response(j_res, x_res):
    """Return the common one in the both of the responses
    comparision will be based on id
    """
    try:
        tree = ET.ElementTree(ET.fromstring(x_res.content))
        x_data = tree.findall('element')
    except Exception as e:
        print(e)
        print("error parsing xml response")
        sys.exit()
    try:
        j_data = j_res.json()["hotels"]
    except Exception as e:
        print(e)
        print("error parsing json response")
        sys.exit()


    # now try to only keep the common ones
    xml_ids = set([x.find("id").text for x in x_data])  # record all the ids in xml_ids response

    common_json = [j for j in j_data if str(j['id']) in xml_ids]
    print("There are {} common ones that appeared on both response".format(len(common_json)))
    print("common ids are: {}".format(sorted([j['id'] for j in common_json])))

    return common_json


def write_data_to_db(json_data):

    def create_connection(db_file):
        conn = None
        try:
            conn = sqlite3.connect(db_file)
        except Exception as e:
            print("create connection to database failed")
            print(e)
            sys.exit()
        return conn

    def create_hotel_table(conn):
        if not conn:
            print("passing invalid connection to create_hotel_table")
            print(conn)
            sys.exit()
        cur = conn.cursor()
        cur.execute(SQL_CREATE_TABLE)
        return cur.lastrowid

    def create_hotel_entry(conn, hotel):
        if not conn:
            print("passing invalid connection to create_hotel_entry")
            print(conn)
            sys.exit()
        cur = conn.cursor()
        cur.execute(SQL_INSERT_DATA, hotel)
        return cur.lastrowid

    conn = create_connection(DB_NAME)
    with conn:
        # first, create table in database
        try:
            create_hotel_table(conn)
        except Exception as e:
            print(e)
            print("Exception happended when creating hotel table")

        # insert common data to database
        for j in json_data:
            try:
                hotel = (
                    j['id'],
                    j['hotel_name'],
                    j['num_reviews'],
                    j['address'],
                    j['num_stars'],
                    "#".join(j['amenities']),
                    j['image_url'],
                    j['price']
                )
                create_hotel_entry(conn, hotel)
            except Exception as e:
                print(e)
                print("Excepiton happened when inserting {}".format(j))
                print("id of the hotel {}".format(j['id']))


if __name__ == "__main__":
    # rm the db file before each run to make validate results easy
    print("Step 0, clean up before each run...")
    if os.path.exists(DB_NAME):
        os.remove(DB_NAME)

    ### Step 1, read input
    print("Step 1, reading input.....")
    parser = ArgumentParser()
    parser.add_argument("-c", "--city", dest="city_string",
                        help="City String")
    parser.add_argument("-ci", "--checkin", dest="check_in",
                        help="Check in string")
    parser.add_argument("-co", "--checkout", dest="check_out",
                        help="Check out string")
    args = parser.parse_args()

    ### step 2, make parallel requests
    print("Step 2, sending input.....")
    manager = Manager()
    return_dict = manager.dict()        # use shared variable to capture output
    p_json, p_xml = Process(target=make_json_request,
                            args=(args.city_string, args.check_in, args.check_out, return_dict)), \
                    Process(target=make_xml_request,
                            args=(args.city_string, args.check_in, args.check_out, return_dict))
    p_json.start()
    p_xml.start()

    p_json.join()
    p_xml.join()

    # step 3, parse the data and save to database
    print("Step 3, parse response and save ....")
    common_json = parse_response(return_dict['json_response'], return_dict['xml_response'])
    print("we will be using sqlite3 as our sql database")

    write_data_to_db(common_json)
    print("writing to database complete.... DB path: {}".format(DB_NAME))
    print("exiting...")
